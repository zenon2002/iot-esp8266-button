#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

#include <ESP8266HTTPClient.h>

ESP8266WebServer server(80);

const char *ssid = "test";
const char *passphrase = "test";
String st;
String content;
int statusCode;
String target = "";

void setup()
{
  pinMode(2, OUTPUT);
  pinMode(0, OUTPUT);
  digitalWrite(2, HIGH);
  Serial.begin(115200);
  EEPROM.begin(512);
  delay(10);
  Serial.println();
  Serial.println();
  Serial.println("Startup");
  // read eeprom for ssid and pass
  Serial.println("Reading EEPROM ssid");
  String esid;
  for (int i = 0; i < 32; ++i)
  {
    esid += char(EEPROM.read(i));
  }
  Serial.print("SSID:  ->");
  Serial.print(esid);
  Serial.println("<-");
  Serial.println("Reading EEPROM pass");
  String epass = "";
  for (int i = 32; i < 96; ++i)
  {
    epass += char(EEPROM.read(i));
  }
  Serial.print("PASS: ->");
  Serial.print(epass);
  Serial.println("<-");
  Serial.println("Reading EEPROM target");
  String etarget = "";
  for (int i = 96; i < 160; ++i)
  {
    etarget += char(EEPROM.read(i));
  }
  Serial.print("TARGET: ->");
  Serial.print(etarget);
  Serial.println("<-");
  target = etarget.c_str();
  Serial.println(strlen(esid.c_str()));
  if (strlen(esid.c_str()) > 1)
  {
    WiFi.begin(esid.c_str(), epass.c_str());
    if (testWifi())
    {
      launchWeb(0);
      return;
    }
  }
  setupAP();
}

bool testWifi(void)
{
  int c = 0;
  boolean high = false;
  Serial.println("Waiting for Wifi to connect");
  while (c < 100)
  {
    if (WiFi.status() == WL_CONNECTED)
    {
      digitalWrite(0, LOW);
      return true;
    }
    if (high)
    {
      digitalWrite(0, LOW);
      high = false;
    }
    else
    {
      digitalWrite(0, HIGH);
      high = true;
    }
    delay(500);
    Serial.print(WiFi.status());
    c++;
  }
  Serial.println("");
  Serial.println("Connect timed out, opening AP");
  digitalWrite(0, HIGH);
  return false;
}

void launchWeb(int webtype)
{
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("SoftAP IP: ");
  Serial.println(WiFi.softAPIP());
  createWebServer(webtype);
  // Start the server
  server.begin();
  Serial.println("Server started");
}

void setupAP(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
  st = "<ol>";
  for (int i = 0; i < n; ++i)
  {
    // Print SSID and RSSI for each network found
    st += "<li>";
    st += WiFi.SSID(i);
    st += " (";
    st += WiFi.RSSI(i);
    st += ")";
    st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
    st += "</li>";
  }
  st += "</ol>";
  delay(100);
  WiFi.softAP(ssid, passphrase, 6);
  Serial.println("softap");
  launchWeb(1);
  Serial.println("over");
}

void createWebServer(int webtype)
{
  if (webtype == 1)
  {
    server.on("/", []() {
      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      content = "<!DOCTYPE HTML>\r\n<html>Hello from ESP8266 at ";
      content += ipStr;
      content += "<p>";
      content += st;
      content += "</p><form method='get' action='setting'>";
      content += "<label for='ssid' style='display:inline-block; width: 100px;'>SSID: </label>";
      content += "<input name='ssid' length=32 ><br/>";
      content += "<label for='pass' style='display:inline-block; width: 100px;'>Password: </label>";
      content += "<input name='pass' length=64><br/>";
      content += "<label for='target' style='display:inline-block; width: 100px;'>Target: </label>";
      content += "<input name='target' length=64><br/><br/>";
      content += "<input type='submit'></form>";
      content += "</html>";
      server.send(200, "text/html", content);
    });
    server.on("/setting", []() {
      String qsid = server.arg("ssid");
      String qpass = server.arg("pass");
      String qtarget = server.arg("target");
      if (qsid.length() > 0 && qpass.length() > 0 && qtarget.length() > 0)
      {
        Serial.println("clearing eeprom");
        for (int i = 0; i < 160; ++i)
        {
          EEPROM.write(i, 0);
        }
        Serial.println(qsid);
        Serial.println("");
        Serial.println(qpass);
        Serial.println("");

        Serial.println("writing eeprom ssid:");
        for (int i = 0; i < qsid.length(); ++i)
        {
          EEPROM.write(i, qsid[i]);
          Serial.print("Wrote: ");
          Serial.println(qsid[i]);
        }
        Serial.println("writing eeprom pass:");
        for (int i = 0; i < qpass.length(); ++i)
        {
          EEPROM.write(32 + i, qpass[i]);
          Serial.print("Wrote: ");
          Serial.println(qpass[i]);
        }
        Serial.println("writing eeprom target:");
        for (int i = 0; i < qtarget.length(); ++i)
        {
          EEPROM.write(96 + i, qtarget[i]);
          Serial.print("Wrote: ");
          Serial.println(qtarget[i]);
        }
        EEPROM.commit();
        content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
        statusCode = 200;
      }
      else
      {
        content = "{\"Error\":\"404 not found\"}";
        statusCode = 404;
        Serial.println("Sending 404");
      }
      server.send(statusCode, "application/json", content);
    });
  }
  else if (webtype == 0)
  {

    server.on("/", []() {
      IPAddress ip = WiFi.localIP();
      content = "<!DOCTYPE HTML>\r\n<html>";
      content += "<p>current target:<br/>";
      content += target;
      content += "</p><p>";
      content += "<a href='cleareeprom'>clear memory</a>";
      "</p></html>";
      server.send(200, "text/html", content);
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      server.send(200, "application/json", "{\"IP\":\"" + ipStr + "\"}");
    });
    server.on("/cleareeprom", []() {
      content = "<!DOCTYPE HTML>\r\n<html>";
      content += "<p>Clearing the EEPROM</p></html>";
      server.send(200, "text/html", content);
      Serial.println("clearing eeprom");
      for (int i = 0; i < 160; ++i)
      {
        EEPROM.write(i, 0);
      }
      EEPROM.commit();
    });

    HTTPClient http;
    http.begin(target); //HTTP
    int httpCode = http.GET();

    // httpCode will be negative on error
    if(httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if(httpCode == HTTP_CODE_OK) {
          digitalWrite(0, HIGH);
          delay(100);
          digitalWrite(0, LOW);
          delay(100);
          digitalWrite(0, HIGH);
          delay(100);
          digitalWrite(0, LOW);
          delay(100);
          digitalWrite(0, HIGH);
          delay(100);
          digitalWrite(0, LOW);
          delay(100);
          digitalWrite(0, HIGH);
          delay(100);
          digitalWrite(0, LOW);
          delay(100);
          digitalWrite(0, HIGH);
          delay(100);
          digitalWrite(0, LOW);
          delay(100);
          digitalWrite(2, LOW);
        }
    } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }
    
    http.end();
  }
}

void loop()
{
  server.handleClient();
}
