# README #

This is the code for a IoT Button built with an ESP8266 micro controller. Idea based on the [IFTTT Smart Button](https://www.hackster.io/noelportugal/ifttt-smart-button-e11841) project of [Noel Portugal](https://www.hackster.io/noelportugal) at hackster.io. Thank you Noel!
I added a status led and write down the code in ```c```


## Things to Do ##
1.  open file with Arduino IDE
2. flash the ESP8266
3. have fun

### What is this repository for? ###

This firmware provides a configurable and easy to use IoT-Button based on ESP8266.

## Hardware ##

* 1 ESP8266 ESP01
* 1 simple button
* 1 1N4001 diode
* 2 1k resistor
* 1 low current status led
* some electric wire
* 2 1.5V batteries (AA or R6)

![iot_esp8266.PNG](https://bitbucket.org/repo/Lor946o/images/2624839964-iot_esp8266.PNG)